
let urlTeams = "https://api.football-data.org/v2/competitions/2002/teams?X-Auth-Token=8753f4c1708a444fa2f280336ea705ad";

fetch(urlTeams, {

    method:"GET",
    headers:{
        "x-auth-token": "8753f4c1708a444fa2f280336ea705ad"
    }
})

  .then(response => response.json())
  .then(function (data) {
    let html=""  
    data.teams.forEach(element => {
          html += "<li onClick='infoLoad(this)' team-id=" +element.id + " founded=" + element.founded + " fullName='" + element.name + " 'website='" + element.website + "' hq='" + element.address + "' phoneNR='" + element.phone + "'email='" + element.email + "'> <img id='image' src='" + element.crestUrl + "'/>" + element.shortName + "</li>";
        });
        html = "<ul id='menu'>" + html + "</ul>"
      document.getElementById("teams").innerHTML = html;

      document.getElementById("background2").style.display = "none";
      document.getElementById("returnButton").style.display = "none";
      
      
  });
/*
document.getElementById("teams").addEventListener("click",function()
{
    infoLoad(this)
})*/

function infoLoad(element) {

   
    document.getElementById("background2").style.display = "none"; 
    document.getElementById("background").style.display = "none"; 

    console.log(element)
    let team_id = element.attributes['team-id'].value
    let img_src = element.children[0].getAttribute('src')
    let full_name = element.attributes['fullName'].value
    let founded = element.attributes['founded'].value
    let hq = element.attributes['hq'].value
    let phoneNR = element.attributes['phoneNR'].value
    let email = element.attributes['email'].value
    let website = element.attributes['website'].value

    console.log(team_id);
    
    console.log("src="+img_src)
    let team_info = "<h1 class='teamname'>" + "Team: " + element.innerText +"</h1>" +
    "<p> <img id=detailImage src='" + img_src + "'/> </p>" + "<ul id='details'>" +
     "<li>" + "<u>Vollständiger Name</u>: " + full_name +  "</li>" + 
     "<li>" + "<u>Gründungsdatum</u>: " + founded  + "</li>" +
     "<li>" + "<u>Hauptsitz</u>: " + hq  + "</li>" +
     "<li>" + "<u>Website</u>: <a href='" + website + "'>" + website  + "</li></a>" +
     "<li>" + "<u>Kontakt</u>: " + phoneNR  + " | " + email + "</li>" +
     "</ul>";

    let playerList = 
    "<li>spieler1</li>"+
    "<li>spieler2</li>"+
    "<li>spieler3</li>"+
    "<li>spieler4</li>"+
    "<li>spieler5</li>"+
    "<li>spieler6</li>";
     
    
    document.getElementById("playerList").innerHTML = playerList;
    
    //teams:
    document.getElementById("teams").innerHTML = team_info;
    document.getElementById("teams").style.marginLeft = "50px";
    document.getElementById("teams").style.paddingTop = "20px";


    urlPlayers = "http://api.football-data.org/v2/teams/" + team_id
  
    fetch(urlPlayers, {

      method:"GET",
      headers:{
          "x-auth-token": "8753f4c1708a444fa2f280336ea705ad"
      }
  })
  
    .then(response => response.json())
    .then(function (data) {
      let player_info=""  
      data.squad.forEach(element => {

          Rolle = element.role.toLowerCase();
          Rolle = Rolle.replace(Rolle.charAt(0),Rolle.charAt(0).toUpperCase());

          if (Rolle == "Player") {
            Rolle = "- " + element.position;
          }
          
          else {
            Rolle = "- " + Rolle; 
          }

          if (Rolle.includes("_") == true) {
            Rolle = Rolle.replace("_","");
          }
          
          


            player_info += "<li>" + "<p1>" + element.name + "</p1>" + " " + Rolle + "</li>";
          });
          player_info = "<ul id='players'>" + player_info + "</ul>"

          console.log("HIER: " + player_info);


          document.getElementById("returnButton").style.display = "";
          document.getElementById("background2").style.display = ""; //Show the second background
          document.getElementById("background").style.display = ""; 
          document.getElementById("playerList").innerHTML = player_info;
    });
    
}
