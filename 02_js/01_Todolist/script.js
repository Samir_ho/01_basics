//JSON javascript object notation
var task1 = {name: "putzen", isDone:false, responsible:"Andreas"};
var task2 = {name: "schwimmen", isDone:true, responsible:"Andreas"};

const tasks = [];
console.log(tasks)

printTasks();


document.getElementById("Tasklist").innerHTML = getHTMLTasks();

document.getElementById("addTask").addEventListener("click",function(){
    addTask();
});

function addTask(){
    let taskName = document.getElementById("txtnewTask").value;
    let taskResponsible = document.getElementById("txtresponsible").value;
    let task = {name:taskName, responsible: taskResponsible, isDone:false};
    tasks.push(task)
    printTasks();
}

function printTasks(){
    document.getElementById("Tasklist").innerHTML = getHTMLTasks();
    if (tasks.length <= 0){

        document.getElementById("Tasklist").innerHTML = "<li>No Tasks added yet </li>";
    }
}

function markTask(element){
    let index = element.attributes["data-index"].value;
    let isChecked = element.checked;
    
    tasks[index].isDone = isChecked;
    printTasks();
    //alert("marked" + " " + isChecked + " " + index);
}

function getHTMLTasks(){
    let html = "";
    let index = 0;
    let taskNr = 1;
    
    tasks.forEach(element => {
        let checked = "";
        if (element.isDone)
        {
            checked = "checked";
        }
        
        html += "<li>" + "Nr. " + taskNr + " | " + element.name + " - Verantwortlicher: " + element.responsible + " " + "<input onClick='markTask(this)' type = 'checkbox' data-index=" + index + " " + checked + "/>" + "-Erledigt?" + '<button1 id =delButton onClick="removeTask(this)" data-index=' + index + ">" + '<img src="../Bilder/Trash_font_awesome.svg.png" ></button>' + "</li>";
        index++;
        taskNr++;
    
        });
    return html;
}

function onload(){
    if (tasks.length <= 0){

        document.getElementById("Tasklist").innerHTML = "<li>No Tasks added yet </li>";
    }
}

function removeTask(zuLöschen){
    let index = zuLöschen.attributes["data-index"].value;
    tasks.splice(index,1)
    printTasks()
    
}

